import time

currenttime = time.strftime("%d-%m-%Y | %H:%M:%S")

# The list
shopping_list = []

# Instructions
print("What to buy")
print ("Enter 'DONE' to stop")

while True:
    new_item = input("> ")

    #Break
    if new_item == 'DONE':
        break
    #Add items
    shopping_list.append(new_item)

#Print list
print("Here's the list:")

for item in shopping_list:
    print(item)
print("---")
print("List items:", len(shopping_list))
print("Generated:" , currenttime)

with open('ShoppingList.txt', 'w') as f:
    f.write("Items to buy: \n \n")
    for item in shopping_list:
        f.write("%s\n" % item)
        
